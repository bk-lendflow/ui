<?php

namespace LendFlow\UI\Solutions;

use Illuminate\Support\Facades\Artisan;
use Facade\IgnitionContracts\RunnableSolution;

class UIAssetsNotFoundSolution implements RunnableSolution
{
    public function getSolutionTitle(): string
    {
        return 'LendFlow UI assets are missing';
    }

    public function getSolutionDescription(): string
    {
        return 'Publish the LendFlow UI assets using `php artisan vendor:publish --tag=lendflow::ui`.';
    }

    public function getDocumentationLinks(): array
    {
        return ['README.md' => 'https://gitlab.com/bk-lendflow/app#publishing-ui'];
    }

    public function getSolutionActionDescription(): string
    {
        return 'Click the button below to let the app run the above artisan command for you.';
    }

    public function getRunButtonText(): string
    {
        return 'Publish LendFlow UI assets';
    }

    public function run(array $parameters = [])
    {
        Artisan::call('vendor:publish', $parameters);
    }

    public function getRunParameters(): array
    {
        return ['--tag' => 'lendflow::ui'];
    }
}
